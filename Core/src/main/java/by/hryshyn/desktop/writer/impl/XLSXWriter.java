/*
 * @(#)XLSXWriter.java   1.0 2017/06/17
 */
package by.hryshyn.desktop.writer.impl;

import by.hryshyn.desktop.exceptions.WriterException;
import by.hryshyn.desktop.model.Record;
import by.hryshyn.desktop.writer.IWriter;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * A xlsx writer is an output stream for writing Record list to .xlsx file
 * 
 * @version 1.0 17 June 2017
 * @author Pavel Hryshyn
 */
public class XLSXWriter implements IWriter {
	private final static Logger logger = Logger.getLogger(XLSXWriter.class);

	/* Sheet name property */
	private final static String SHEET_NAME = "results";

	/**
	 * Writes Record list to .xlsx file
	 * 
	 * @param fileName
	 *            - the system-dependent file name
	 * @param records
	 *            - list of records for writing
	 * @return count - count of written records
	 * @throws WriterException
	 *             - if an FileNotFound or I/O error occurs
	 */
	@Override
	public int write(String fileName, List<Record> records)
			throws WriterException {
		int rownum = 0;
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(SHEET_NAME);
		for (Record record : records) {
			addRecordToSheet(record, sheet, rownum);
			rownum++;
		}
		writeXSLXFile(fileName, workbook);
		return rownum;
	}

	private void addRecordToSheet(Record record, Sheet sheet, int rownum) {
		int size = record.getAttributes().size();
		Row row = sheet.createRow(rownum);

		for (int cellnum = 0; cellnum < size; cellnum++) {
			Object value = record.getAttributes().get(cellnum);
			Cell cell = row.createCell(cellnum);
			if (value != null) {
				cell.setCellValue(value.toString());
			}
		}
	}

	// Write the workbook in the file system
	private void writeXSLXFile(String fileName, Workbook workbook)
			throws WriterException {
		try (FileOutputStream out = new FileOutputStream(new File(fileName))) {
			workbook.write(out);
			logger.info("File with name " + fileName + " is written");
		} catch (FileNotFoundException e) {
			throw new WriterException("File isn't found. FileName = "
					+ fileName, e);
		} catch (IOException e) {
			throw new WriterException(
					"IOException occured in the writing time", e);
		}
	}
}
