/*
 * @(#)IWriter.java   1.0 2017/06/17
 */
package by.hryshyn.desktop.writer;

import by.hryshyn.desktop.exceptions.WriterException;
import by.hryshyn.desktop.model.Record;

import java.util.List;

/**
 * A writer is an output stream for writing Record list to file
 * 
 * @version 1.0 17 June 2017
 * @author Pavel Hryshyn
 */
public interface IWriter {

	/**
	 * Writes Record list to file
	 * 
	 * @param fileName
	 *            - the system-dependent file name
	 * @param records
	 *            - list of records for writing
	 * @return count - count of written records
	 * @throws WriterException
	 *             - if any error occurs in writing time
	 */
	int write(String fileName, List<Record> records) throws WriterException;
}
