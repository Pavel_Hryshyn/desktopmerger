/*
* @(#)Merger.java   1.0 2017/06/18
*/
package by.hryshyn.desktop.merger.impl;

import by.hryshyn.desktop.merger.IMerger;
import by.hryshyn.desktop.model.Record;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Define methods for merging Record lists
 * @version 1.0 18 June 2017
 * @author Pavel Hryshyn
 */
public class RecordsByKeyMerger implements IMerger {

    Logger logger = Logger.getLogger(RecordsByKeyMerger.class);

    @Override
    public List<Record> merge(List<Record> from, List<Record> to, int[] mergedIndexes) {
        logger.info("Records merging started");
        List<Record> mergedRecords = new ArrayList<Record>(to);
        int toAttributesSize = mergedRecords.get(0).getAttributes().size();
        for (Record fromRecord: from) {
            findRecordForMerge(fromRecord, mergedRecords, toAttributesSize, mergedIndexes);
        }
        logger.info("Records merging finished");
        return mergedRecords;
    }

    private void findRecordForMerge(Record from, List<Record> to, int toAttrSize, int[] mergedIndexes) {
        for (Record merged: to) {
            if (merged.getKey() != null && from.getKey() != null
                    && merged.getKey().equalsIgnoreCase(from.getKey())) {
                int mergingIndex = merged.getAttributes().size();
                if (mergingIndex <= toAttrSize) {
                    for (int fromIndex : mergedIndexes) {
                        merged.getAttributes().put(mergingIndex, from.getAttributes().get(fromIndex));
                        mergingIndex++;
                    }
                }
            }
        }
    }
}
