/*
* @(#)IMerger.java   1.0 2017/06/18
*/
package by.hryshyn.desktop.merger;

import by.hryshyn.desktop.model.Record;

import java.util.List;

/**
 * Define methods for merging Record lists
 * @version 1.0 18 June 2017
 * @author Pavel Hryshyn
 */
public interface IMerger {

    /**
     * Merge 2 Record list by (from) indexes
     * @param from
     * @param to
     * @param mergedIndexes
     * @return
     */
    List<Record> merge(List<Record> from, List<Record> to, int[] mergedIndexes);
}
