/*
* @(#)Record.java   1.0 2017/06/15
*/
package by.hryshyn.desktop.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Defines model of Excel Row
 * @version 1.0 15 June 2017
 * @author Pavel Hryshyn
 */
public class Record {

    /** Record key */
    private String key;

    /** Stores all model attributes */
    private Map<Integer, Object> attributes;

    public Record() {
        super();
        this.attributes = new HashMap<Integer, Object>();
    }

    public Record(String key) {
        super();
        this.key = key;
        this.attributes = new HashMap<Integer, Object>();
    }

    /**
     * Put attribute to map
     * @param seq
     * @param value
     */
    public void addAttribute(Integer seq, Object value){
        attributes.put(seq, value);
    }

    /**
     * Remove attribute from map
     * @param seq
     */
    public void deleteAttribute(Integer seq){
        attributes.remove(seq);
    }

    /**
     * Return Record key
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Set key
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Return attribites
     * @return map
     */
    public Map<Integer, Object> getAttributes() {
        return attributes;
    }

    /**
     * Set attributes for Record
     * @param attributes
     */
    public void setAttributes(Map<Integer, Object> attributes) {
        this.attributes = attributes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (key != null ? !key.equals(record.key) : record.key != null) return false;
        return attributes != null ? attributes.equals(record.attributes) : record.attributes == null;

    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (attributes != null ? attributes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Record{" +
                "key='" + key + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}
