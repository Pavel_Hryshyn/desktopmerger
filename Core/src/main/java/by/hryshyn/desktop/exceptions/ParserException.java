/*
 * @(#)ParserException.java   1.0 2017/08/07
 */
package by.hryshyn.desktop.exceptions;

/**
 * Signals that an exception of file parsing has occurred
 * 
 * @author Pavel Hryshyn
 * @version 1.0 07 August 2017
 */
public class ParserException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an ParserException with null as 
	 * its error detail message.
	 */
	public ParserException() {
	}

	/**
	 * Constructs an ParserException with 
	 * the specified detail message and cause
	 * @param message - The detail message
	 * @param cause - The cause
	 */
	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}
}
