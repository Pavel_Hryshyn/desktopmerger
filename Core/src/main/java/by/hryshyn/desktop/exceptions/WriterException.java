/*
 * @(#)WriterException.java   1.0 2017/08/07
 */
package by.hryshyn.desktop.exceptions;

/**
 * Signals that an exception of file writing has occurred
 * 
 * @author Pavel Hryshyn
 * @version 1.0 07 August 2017
 */
public class WriterException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs an WriterException with null as 
	 * its error detail message.
	 */
	public WriterException() {
	}

	/**
	 * Constructs an WriterException with 
	 * the specified detail message and cause
	 * @param message - The detail message
	 * @param cause - The cause
	 */
	public WriterException(String message, Throwable cause) {
		super(message, cause);
	}
}
