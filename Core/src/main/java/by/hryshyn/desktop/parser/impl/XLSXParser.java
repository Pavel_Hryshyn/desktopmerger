/*
* @(#)XLSXParser.java   1.0 2017/06/15
*/
package by.hryshyn.desktop.parser.impl;

import by.hryshyn.desktop.exceptions.ParserException;
import by.hryshyn.desktop.model.Record;
import by.hryshyn.desktop.parser.IFileParser;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A xlsx parser parses .xlsx files to Records list
 * @version 1.0 15 June 2017
 * @author Pavel Hryshyn
 */
public class XLSXParser implements IFileParser {
    private final static Logger logger = Logger.getLogger(XLSXParser.class);
    
    /* Empty string const */
    private static String EMPTY_STRING = "";

    /**
     * 
     */
    @Override
    public List<Record> parse(String file, Integer keyPosition) throws ParserException {
        logger.info("Start parse file: " + file);
        File xlsxFile = new File(file);
        List<Record> records = new ArrayList<>();
        try (InputStream in = new FileInputStream(xlsxFile);
             XSSFWorkbook workbook = new XSSFWorkbook(in)) {

            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Record record = parseRow(row, keyPosition);
                records.add(record);
            }
            logger.info("Parsing is finished successfully. Record count is " + records.size());
        } catch (FileNotFoundException e) {
           throw new ParserException("File haven't found", e);
        } catch (IOException e) {
            throw new ParserException("Cannot create workbook", e);
        }
        return records;
    }

    private Record parseRow(Row row, Integer keyPosition) {
        int position = 0;
        Record record = new Record();
        for (int cellNum = 0; cellNum < row.getLastCellNum(); cellNum++)
        {
            Object recordAttribute = null;
            XSSFCell cell = (XSSFCell)row.getCell(cellNum, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

            switch (cell.getCellTypeEnum())
            {
                case NUMERIC:
                    recordAttribute = cell.getNumericCellValue();
                    break;
                case STRING:
                    recordAttribute = cell.getStringCellValue();
                    break;
                case FORMULA:
                    recordAttribute = cell.getCellFormula();
                    break;
                case BLANK:
                    recordAttribute = EMPTY_STRING;
                    break;
                default:
                    recordAttribute = EMPTY_STRING;
                    break;
            }
            record.addAttribute(position, recordAttribute);
            if (position == keyPosition){
                record.setKey(cell.toString());
            }
            position++;
        }
        return record;
    }
}
