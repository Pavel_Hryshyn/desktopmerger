/*
* @(#)IFileParser.java   1.0 2017/06/15
*/
package by.hryshyn.desktop.parser;

import by.hryshyn.desktop.exceptions.ParserException;
import by.hryshyn.desktop.model.Record;

import java.util.List;

/**
 * Defines methods for parsing files to models
 * @version  1.0 15 June 2017
 * @author Pavel Hryshyn
 */
public interface IFileParser {

    /**
     * Parse file to list of Records
     * @param file path to file
     * @param keyPosition position of Record key
     * @return records
     */
    List<Record> parse(String file, Integer keyPosition) throws ParserException;
}
