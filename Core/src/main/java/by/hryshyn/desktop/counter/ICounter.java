/*
* @(#)ICounter.java   1.0 2017/06/20
*/
package by.hryshyn.desktop.counter;

import by.hryshyn.desktop.model.Record;

import java.util.List;

/**
 * Define methods for counting Records
 * @version 1.0 20 June 2017
 * @author Pavel Hryshyn
 */
public interface ICounter {
    /**
     * Count Record list to file
     * @param records
     * @param position
     * @return count count of written records
     */
    List<Record> count(List<Record> records, int position);
}
