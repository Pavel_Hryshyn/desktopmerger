/*
* @(#)UniqueDataInColumnCounter.java   1.0 2017/06/20
*/
package by.hryshyn.desktop.counter.impl;

import by.hryshyn.desktop.counter.ICounter;
import by.hryshyn.desktop.model.Record;

import java.util.*;

/**
 * Define methods for counting Records
 * @version 1.0 20 June 2017
 * @author Pavel Hryshyn
 */
public class UniqueDataInColumnCounter implements ICounter {


    @Override
    public List<Record> count(List<Record> records, int position) {
        List<Record> countedRecords = new ArrayList<>();
        Map<String, Set<String>> groupMap = new LinkedHashMap<>();

        for (Record record: records) {
            Set<String> unique = null;
            if (groupMap.get(record.getKey()) == null) {
                unique = new HashSet<String>();
                String email = (String) record.getAttributes().get(position);
                unique.add(email);
                groupMap.put(record.getKey(), unique);
            } else {
                unique = groupMap.get(record.getKey());
                unique.add((String) record.getAttributes().get(position));
            }
        }
        Set<String> keySet = groupMap.keySet();
        for (String key: keySet) {
            Set<String> setEmails = groupMap.get(key);
            Record record = new Record(key);
            record.addAttribute(0, key);
            record.addAttribute(1, setEmails.size());
            countedRecords.add(record);
        }
        return countedRecords;
    }
}
