/*
* @(#)RecordsByKeyMergerTest.java   1.0 2017/06/19
*/
package by.hryshyn.desktop.merger.impl;

import by.hryshyn.desktop.merger.IMerger;
import by.hryshyn.desktop.model.Record;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 *  Test for RecordsByKeyMerger
 *  @author Pavel Hryshyn
 *  @version 1.0 19 June 2017
 */
public class RecordsByKeyMergerTest {

    @Test
    public void testMerge() {
        IMerger merger = new RecordsByKeyMerger();
        int expectedRecordsSize = 10;
        int[] mergedIndexes = {0, 2, 3};
        List<Record> mergedRecords = merger.merge(createFromRecords(), createToRecords(), mergedIndexes);
        assertEquals(expectedRecordsSize, mergedRecords.size());
        for (int i=0; i < 5; i++) {
            int actualAttributesSize = mergedRecords.get(i).getAttributes().size();
            int expectedAttributesSize = 8;
            assertEquals(expectedAttributesSize, actualAttributesSize);
        }
        assertEquals(5, mergedRecords.get(5).getAttributes().size());
    }

    private List<Record> createFromRecords() {
        List<Record> records = new ArrayList<>();
        for (int i=0; i < 3; i++) {
            records.add(createRecord("key1", 5));
        }
        records.add(createRecord("key9", 5));
        records.add(createRecord("key3", 5));
        records.add(createRecord("key7", 5));
        records.add(createRecord("key12", 5));
        records.add(createRecord("key6", 5));
        return records;
    }

    private List<Record> createToRecords() {
        List<Record> records = new ArrayList<>();
        for (int i=0; i < 5; i++) {
            records.add(createRecord("key1", 5));
        }
        records.add(createRecord("key2", 5));
        records.add(createRecord("key3", 5));
        records.add(createRecord("key2", 5));
        records.add(createRecord("key5", 5));
        records.add(createRecord("key6", 5));
        return records;
    }

    private Record createRecord(String key, int numberOfAttributes) {
        Record record = new Record();
        Map<Integer, Object> attributes = createRecordAttributes(numberOfAttributes);
        record.setKey(key);
        record.setAttributes(attributes);
        return record;
    }

    private Map<Integer,Object> createRecordAttributes(int numberOfAttributes) {
        Map<Integer,Object> attributes = new HashMap<Integer, Object>();
        for (int i=0; i < numberOfAttributes; i++) {
            attributes.put(i, "Attribute" + i);
        }
        return attributes;
    }
}
