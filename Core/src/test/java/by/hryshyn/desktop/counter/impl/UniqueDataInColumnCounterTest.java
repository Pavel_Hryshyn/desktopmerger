/*
* @(#)UniqueDataInColumnCounterTest.java   1.0 2017/06/20
*/
package by.hryshyn.desktop.counter.impl;

import by.hryshyn.desktop.counter.ICounter;
import by.hryshyn.desktop.model.Record;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 *  Test for UniqueDataInColumnCounter
 *  @author Pavel Hryshyn
 *  @version 1.0 20 June 2017
 */
public class UniqueDataInColumnCounterTest {

    @Test
    public void testCount() {
        ICounter counter = new UniqueDataInColumnCounter();
        List<Record> initialRecords = createRecords();
        int expectedRecordsSize = 2;
        int expectedAttributesSize = 2;
        List<Record> actualRecords = counter.count(initialRecords, 2);
        assertEquals(expectedRecordsSize, actualRecords.size());
        assertEquals(expectedAttributesSize, actualRecords.get(0).getAttributes().size());
        assertEquals(2, actualRecords.get(0).getAttributes().get(1));
        assertEquals(1, actualRecords.get(1).getAttributes().get(1));
    }


    private List<Record> createRecords() {
        List<Record> records = new ArrayList<>();
        records.add(createRecord("Group1", createAttributes(new String[] {"Group1", "test1", "unique1"})));
        records.add(createRecord("Group2", createAttributes(new String[] {"Group2", "test2", "unique2"})));
        records.add(createRecord("Group1", createAttributes(new String[] {"Group1", "test12", "unique11"})));
        records.add(createRecord("Group1", createAttributes(new String[] {"Group1", "test1", "unique1"})));
        records.add(createRecord("Group1", createAttributes(new String[] {"Group1", "test1", "unique1"})));
        records.add(createRecord("Group2", createAttributes(new String[] {"Group2", "test1", "unique2"})));
        return records;
    }

    private Record createRecord(String key, Map<Integer, Object> attributes) {
        Record record = new Record();
        record.setKey(key);
        record.setAttributes(attributes);
        return record;
    }

    private Map<Integer,Object> createAttributes(String[] data) {
        Map<Integer, Object> attributes = new HashMap<>();
        for (int i=0; i < data.length; i++) {
            attributes.put(i, data[i]);
        }
        return attributes;
    }
}
