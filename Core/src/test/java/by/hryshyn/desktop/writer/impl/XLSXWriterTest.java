/*
* @(#)XLSXWriterTest.java   1.0 2017/06/18
*/
package by.hryshyn.desktop.writer.impl;

import by.hryshyn.desktop.exceptions.WriterException;
import static org.junit.Assert.fail;
import by.hryshyn.desktop.model.Record;
import by.hryshyn.desktop.writer.IWriter;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *  Test for XLSXWriter
 *  @author Pavel Hryshyn
 *  @version 1.0 17 June 2017
 */
public class XLSXWriterTest {

    @Test
    public void testWrite(){
        int expectedSize = 10;
        List<Record> records = createRecords();
        String fileName = "src/test/resources/Writed_test_file.xlsx";
        IWriter writer = new XLSXWriter();
        int actualSize = 0;
		try {
			actualSize = writer.write(fileName, records);
		} catch (WriterException e) {
			fail();
		}
        File actualFile = new File(fileName);
        assertEquals(expectedSize, actualSize);
        assertTrue(actualFile.exists());
    }
    
    @Test(expected = WriterException.class)
    public void testWriteWithNonExistFile() throws WriterException {
    	List<Record> records = createRecords();
        String fileName = "TestDisk://test/resources/nonExistFile.xlsx";
        IWriter writer = new XLSXWriter();
        writer.write(fileName, records);
    }
    
    
    @Test
    public void testWriteWithEmtyRecords() {
    	int expectedSize = 0;
    	List<Record> records = new ArrayList<Record>();
        String fileName = "src/test/resources/empty_test_file.xlsx";
        IWriter writer = new XLSXWriter();
        int actualSize = 0;
		try {
			actualSize = writer.write(fileName, records);
		} catch (WriterException e) {
			fail();
		}
		File actualFile = new File(fileName);
        assertEquals(expectedSize, actualSize);
        assertTrue(actualFile.exists());
    }

    private List<Record> createRecords() {
        List<Record> records = new ArrayList<>();
        for (int i=0; i < 10; i++) {
            Record record = new Record();
            Map<Integer, Object> attributes = new HashMap<>();
            attributes.put(0, "test0");
            attributes.put(1, "test1");
            attributes.put(2, "test2");
            record.setKey("Key" + i);
            record.setAttributes(attributes);
            records.add(record);
        }
        return records;
    }

}
