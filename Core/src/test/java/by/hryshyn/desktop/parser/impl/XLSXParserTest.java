/*
* @(#)XLSXParserTest.java   1.0 2017/06/17
*/
package by.hryshyn.desktop.parser.impl;

import by.hryshyn.desktop.exceptions.ParserException;
import static org.junit.Assert.fail;
import by.hryshyn.desktop.model.Record;
import by.hryshyn.desktop.parser.IFileParser;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 *  Test for XLSXParser
 *  @author Pavel Hryshyn
 *  @version 1.0 17 June 2017
 */
public class XLSXParserTest {

    @Test
    public void testParse() {
        int expectedRecords = 10;
        int expectedAttributes = 5;
        IFileParser parser = new XLSXParser();
        List<Record> records = null;
		try {
			records = parser.parse("src/test/resources/Parsing_test_file.xlsx", 0);
		} catch (ParserException e) {
			fail();
		}
        int actualAttributes = records.get(0).getAttributes().size();
        assertEquals(expectedRecords, records.size());
        assertEquals(expectedAttributes, actualAttributes);
    }
}
