package by.hryshyn.desktop.ui.controller;

import java.io.File;
import java.util.List;

import by.hryshyn.desktop.merger.IMerger;
import by.hryshyn.desktop.model.Record;
import by.hryshyn.desktop.parser.IFileParser;
import by.hryshyn.desktop.parser.impl.XLSXParser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

/**
 * Created by Volha on 26-Jun-17.
 */
public class Controller {

	private static final String OPEN_TITLE = "Open Resource File";
	
	private static final String SAVE_TITLE = "Save File";
	
	@FXML
	private Button mergeBtn;

	@FXML
	private Button fromBtn;

	@FXML
	private TextField fromPath;

	@FXML
	private Button toBtn;
	
	@FXML
	private TextField toPath;

	@FXML
	private Button saveBtn;
	
	@FXML
	private TextField savePath;
	
	@FXML
	private TextField fromKey;
	
	@FXML
	private TextField toKey;
	
	@FXML
	private TextField indexes;

	public void merge(ActionEvent event) {
		String fromFilePath = fromPath.getText();
		Integer fromPrimaryKey = Integer.valueOf(fromKey.getText());
		List<Record> from = loadFile(fromFilePath, fromPrimaryKey);
		
		String toFilePath = toPath.getText();
		Integer toPrimaryKey = Integer.valueOf(toKey.getText());
		List<Record> to = loadFile(toFilePath, toPrimaryKey);
	}

	public void openFromFile(ActionEvent event) {
		File file = openFile(OPEN_TITLE);
		if (file != null) {
			fromPath.setText(file.getAbsolutePath());
		}
	}

	public void openToFile(ActionEvent event) {
		File file = openFile(OPEN_TITLE);
		if (file != null) {
			toPath.setText(file.getAbsolutePath());
		}
	}

	public void saveFile(ActionEvent event) {
		File file = openFile(SAVE_TITLE);
		if (file != null) {
			savePath.setText(file.getAbsolutePath());
		}
	}
	
	private File openFile(String title) {
		File file = null;
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(title);
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("Excel", "*.xlsx"),
				new FileChooser.ExtensionFilter("All", "*.*"));
		file = fileChooser.showOpenDialog(null);
		return file;
	}
	
	private List<Record> loadFile(String path, Integer keyPosition) {
		IFileParser parser = new XLSXParser();
		List<Record> records = parser.parse(path, keyPosition);
		return records;
	}
}
